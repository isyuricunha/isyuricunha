
#### database administrator based in brazil.

i solve complex data management challenges by designing, implementing, and maintaining high-performance, scalable, and secure database, cloud and server solutions

**find me on the web:**<br>
[website](https://yuricunha.com), [quick links](https://links.yuricunha.com), [meeting with me](https://cal.com/isyuricunha)

**join in my social media, privacy-first focused:**<br>
[memo](https://memo.yuricunha.com/) - no ad, trackers, email, phone number or others stuff

**link with me:**<br>
[x/twitter](https://twitter.com/isyuricunha), [readcv](https://read.cv/isyuricunha), [mail](mailto:isyuricunha@duck.com), [boo](https://signup.boo.world/jejk), [spotify](https://open.spotify.com/user/22wrcoowop6hb63heywvtaypy?si=e1e818483a1a43a1)

**last two blog post:**<br>
[navigating contradictions in life](https://yuricunha.com/blog/navigating-contradictions-in-life) and [im glad you’re here. dont go anywhere](https://yuricunha.com/blog/im-glad-youre-here-dont-go-anywhere)

**podcast and last ep:**<br>
[yuri cunha - blog speech](https://open.spotify.com/show/2XRQ2mpUbtT0ZqxFVrl0KK) and [eu nasci na geração errada](https://open.spotify.com/episode/720qm2ZXIGVodsPvDUYjU5)

**a sentence to brighten your day:**<br>
    the best time to plant a tree was 20 years ago. the second best time is now
